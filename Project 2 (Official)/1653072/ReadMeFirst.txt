Ho Ten: Tran Kien Quoc
MSSV: 1653072
Lop: 16CLC1
Phien ban Visual Studio su dung: 2010
--
PROJECT 2 - HOÀN THÀNH TẤT CẢ YÊU CẦU
1/ Có toolbar kết hợp với các chức năng và check menu item: Đầy đủ chức năng. 
2/ Có chức năng bổ sung: Tạo text winproc, cut, copy, paste, select all nội dung text.
(Các chức năng cut, copy, paste, select all dùng cho text winproc lẫn draw text trên bitmap winproc).
2/ Chọn được color và font, size,... cho mỗi child bitmap window, lưu trữ riêng.
3/ Vẽ được Line, Rectangle, Ellipse, Text và được lưu trữ riêng màu sắc, font.
Lưu ý text draw: TextDraw được vẽ giống như textbox, nếu chọn màu/font trước khi vẽ text thì đó là
kiểu chung cho các lần vẽ text sau. Nếu như vẽ text, đang gõ và chọn lại màu/font thì lúc này, đó là
kiểu riêng cho chính text mình đang gõ, bấm chuột ngoài khung text để kết thúc gõ => Done. Nếu vẽ
text tiếp thì lúc này màu/font sẽ quay lại kiểu chung trước đó chứ không phải là kiểu riêng của text
cũ.
4/ Paint lại được các object, child window đúng hình dạng, màu sắc, kiểu trước đó.
5/ Move/Resize được các object khi user chọn chức năng Select Object.
6/ Lưu thành công với tên file tự user đặt.
Thư mục gốc của file là ổ D.
Tên file gợi ý lúc hộp thoại save bật: "Noname x.drw" (x thuộc [0,9], bắt đầu ở 0, tăng dần nếu thư 
mục gốc có nhiều file Noname giống nhau. Đến file thứ 11 trở đi, vẫn trùng, tên file gợi ý mặc định 
trở thành "Noname *.drw".
Lưu các đối tượng, cấu trúc, kiểu dáng dưới dạng nhị phân.
Có 3 kiểu file: txt, drw và dat.
File đã Save As thì lần sau là Save, không hiển thị hộp thoại nữa.
Sau khi lưu, tên file được đặt lên tiêu đề bitmap child win đang active.
7/ Mở được file từ ngoài vào và hiển thị các đối tượng theo đúng cấu trúc. Đổi tiêu đề bitmap child 
win thành đúng tên file. Nếu như child window "Quoc.drw" đang active mà user mở lại file đó thì thông
báo Cấm sẽ hiện lên và thực hiện mở file thất bại. Nếu muốn mở lại, tắt bỏ file "Quoc.drw" đang active.

***Repository Bucket: https://bitbucket.org/TKQ/windows-programming/src/master/







