﻿// Project_2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Project_2.h"

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PROJECT_2, szWindowClass, MAX_LOADSTRING);
	LoadString(hInstance, IDS_BITMAP_TITLE, szBitmapTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_BITMAP_CLASS, szBitmapWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT_2));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hWndWin, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	ATOM main, bitmap;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_PROJECT_2);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	main = RegisterClassEx(&wcex);

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= BitmapChildWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BMP_WIN));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szBitmapWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_BMP_WIN));
	bitmap = RegisterClassEx(&wcex);

	return (main && bitmap);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

    hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
       250, 100, 900, 600, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	//-------------QUOCTRAN-------------
	hWndWin = GetActiveWindow();
	static int IDBitmap = 0;
	//static bool line=false, rec=false, ell=false, ;
	static bool toolbar=false;
	static INT prev = ID_DRAW_LINE;
	RECT r;
	hMenu = GetMenu(hWnd);
	//-------------QUOCTRAN-------------

	switch (message)
	{
		case WM_CREATE:
			//Khoi tao CLIENT WINDOW trong FRAME WINDOW de san sang chua cac CHILD WINDOW khac
			CLIENTCREATESTRUCT ccs;
			ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 4);
			ccs.idFirstChild = 50000;
			hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, 
				WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL ,
				0, 0, 0, 0, hWnd, (HMENU) NULL, hInst, (LPSTR)&ccs);
			ShowWindow(hwndMDIClient, SW_SHOW);
			break;
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
				case ID_FILE_NEWBITMAP:
					IDBitmap+=1;
					NewBitmapWnd(IDBitmap);
					break;
				case ID_FILE_SAVE:
					SaveData(hWnd);
					break;
				case ID_FILE_OPEN:
					OpenFile(hWnd);
					break;
				case ID_EDIT_CUT:
					if (TypeShape==SELECTOBJECT && hEditBitmap==NULL)
					{
						ClipboardCut_Copy(hWnd, true); //true: cut | false: copy
						break;
					}
					if (hEditBitmap!=NULL) SendMessage(hEditBitmap, WM_CUT, 0, -1);
					break;
				case ID_EDIT_COPY:
					if (TypeShape==SELECTOBJECT && hEditBitmap==NULL)
					{
						ClipboardCut_Copy(hWnd, false); //true: cut | false: copy
						break;
					}
					if (hEditBitmap!=NULL) SendMessage(hEditBitmap, WM_COPY, 0, -1);
					break;
				case ID_EDIT_PASTE:
					if (TypeShape==SELECTOBJECT && hEditBitmap==NULL)
					{
						ClipboardPaste(hWnd);	
						break;
					}
					if (hEditBitmap!=NULL) SendMessage(hEditBitmap, WM_PASTE, 0, -1);
					break;
				case ID_EDIT_DELETEOBJECT:
					if (TypeShape==SELECTOBJECT && hEditBitmap==NULL) DelObject(hWnd);
					break;
				case ID_EDIT_SELECTALL: 
					//lParam là 1 số >0: Số kí tự sẽ chọn, nếu lParam=-1 thì sẽ chọn tất cả
					if (hEditBitmap!=NULL) SendMessage(hEditBitmap, EM_SETSEL, 0, -1);
					break;
				case ID_WINDOW_TILEVERTICAL:
					SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0L);
					break;
				case ID_WINDOW_TILEHORIZONTAL:
					SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_HORIZONTAL, 0L);
					break;
				case ID_WINDOW_CASCADE:
					SendMessage(hwndMDIClient, WM_MDICASCADE, MDITILE_SKIPDISABLED, 0L);
					break;
				case ID_WINDOW_CLOSEALL:
					for (int i=0; i<bitmap.size(); ++i)
					{
						if (bitmap[i].path!=NULL) delete []bitmap[i].path;
						int size=bitmap[i].var.size();
						for (int j=0; j<size; ++j) delete []bitmap[i].var[j].text;
					}
					IDBitmap=0;
					EnumChildWindows(hwndMDIClient, (WNDENUMPROC) MDICloseProc, 0L);
					break;
				case ID_TOOLBAR_VIEWHIDE:
					if (GetClientRect(hWnd,&r)) ViewHideToolbar(hWnd,hwndMDIClient,lParam,r,toolbar);
					break;
				case ID_FORMAT_CHOOSECOLOR:
					Color(hWnd);
					break;
				case ID_FORMAT_CHOOSEFONT:
					Font(hWnd);
					break;
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				case ID_DRAW_ELLIPSE:
					TypeShape = ELLIPSE;
					CheckMenuItem(hMenu, prev, MF_UNCHECKED | MF_BYCOMMAND);
					CheckMenuItem(hMenu, wmId, MF_CHECKED | MF_BYCOMMAND);
					prev = wmId;
					break;
				case ID_DRAW_LINE:
					TypeShape = LINE;
					CheckMenuItem(hMenu, prev, MF_UNCHECKED | MF_BYCOMMAND);
					CheckMenuItem(hMenu, wmId, MF_CHECKED | MF_BYCOMMAND);
					prev = wmId;
					break;
				case ID_DRAW_RECTANGLE:
					TypeShape = RECTANGLE;
					CheckMenuItem(hMenu, prev, MF_UNCHECKED | MF_BYCOMMAND);
					CheckMenuItem(hMenu, wmId, MF_CHECKED | MF_BYCOMMAND);
					prev = wmId;
					break;
				case ID_DRAW_TYPETEXT:
					TypeShape = TYPETEXT;
					CheckMenuItem(hMenu, prev, MF_UNCHECKED | MF_BYCOMMAND);
					CheckMenuItem(hMenu, wmId, MF_CHECKED | MF_BYCOMMAND);
					prev = wmId;
					break;
				case ID_DRAW_SELECTOBJECT:
					TypeShape = SELECTOBJECT;
					CheckMenuItem(hMenu, prev, MF_UNCHECKED | MF_BYCOMMAND);
					CheckMenuItem(hMenu, wmId, MF_CHECKED | MF_BYCOMMAND);
					prev = wmId;
					break;
				default:
					return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			EndPaint(hWnd, &ps);
			break;
		case WM_SETFOCUS:
			SetFocus(hWnd);
			break;
		case WM_DESTROY:
			for (int i=0; i<bitmap.size(); ++i)
			{
				if (bitmap[i].path!=NULL) 
				{
					delete []bitmap[i].path; 
					bitmap[i].path=NULL;
				}
				int size=bitmap[i].var.size();
				for (int j=0; j<size; ++j) 
					if (bitmap[i].var[j].text!=NULL) 
					{
						delete []bitmap[i].var[j].text; 
						bitmap[i].var[j].text=NULL;
					}
			}

			PostQuitMessage(0);
			break;
		case WM_SIZE:
			GetClientRect(hWnd,&r);
			if (!toolbar) MoveWindow(hwndMDIClient, 0, 0, r.right, r.bottom, TRUE);
			else MoveWindow(hwndMDIClient, 0, r.top+28, r.right, r.bottom, TRUE);
			MoveWindow(hToolBarWnd, 0, 0, r.right, r.bottom, TRUE);
			break;
		default:
			return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


//---------------------------------------QUOCTRAN---------------------------------------
int CheckIDFile()
{
	int i=0;
	for (; i<10; ++i)
	{
		char name[14]="/Noname *.drw";
		name[8]=i+48;
		FILE* file;
		if (file=fopen(name,"r")) fclose(file);
		else return i;
	}
	return -1;
}

void SaveData(HWND hWnd)
{
	HWND tmp = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
	int id=-1;
	for (int i=0; i<bitmap.size(); ++i) 
		if (bitmap[i].hWnd==tmp) { id=i+1; break; }
	
	if (hEditBitmap!=NULL) 
	{
		HDC hdc=GetDC(bitmap[prev_win_id-1].hWnd);
		SetDrawText(bitmap[prev_win_id-1].hWnd, hdc, prev_win_id);
	}
	
	ArrayShape a = bitmap[id-1];

	if (bitmap[id-1].path==NULL)
	{
		OPENFILENAME ofn;
		TCHAR  szFile[256] = L"Noname ", namefile[256], rootpath[3] = L"D:";
		int checkidfile=CheckIDFile();
		if (checkidfile!=-1) szFile[7]=checkidfile+48;
	
		TCHAR  szFilter[] = TEXT("Draw (*.drw)\0* .drw\0Text (*.txt)\0 *.txt\0Data (*.dat)\0 *.dat\0");

		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;  //Handle của window cha
		ofn.lpstrFilter = szFilter;
		ofn.nFilterIndex = 1;
		ofn.lpstrFile = szFile;  //Chuỗi đường dẫn + tên file trả về
		ofn.nMaxFile = sizeof(szFile); //Kích cỡ chuỗi szFile
		ofn.lpstrFileTitle = namefile; //Chuỗi tên file trả về
		ofn.nMaxFileTitle = 256; //Kích cỡ chuỗi namefile
		ofn.lpstrInitialDir = rootpath; //Lưu địa chỉ rootpath (địa chỉ mặc định khi lưu)
		ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
		ofn.lpstrDefExt = L"drw";
		
		if (GetSaveFileName(&ofn))
		{
			ofstream f;
			f.open(szFile, ios::binary);

			bitmap[id-1].path=new WCHAR[256];
			//Copy đường dẫn + tên file từ dialog open sang biến bitmap path
			wcscpy(bitmap[id-1].path, ofn.lpstrFile);
			//Đổi tên file
			SetWindowText(bitmap[id-1].hWnd, ofn.lpstrFileTitle);

			if (a.var.size()==0) 
			{
				TCHAR k='-'; 
				f.write((char*)&k, sizeof(TCHAR)); 
				f.close(); 
				return; 
			}

			int size=a.var.size();
			f.write((char*)&size, sizeof(int));
			f.write((char*)&a.oldcolor, sizeof(COLORREF));
			for (int i=0; i<a.var.size(); ++i)
			{
				f.write((char*)&a.var[i].type, sizeof(int)); 
				f.write((char*)&a.var[i].down.x, sizeof(int)); f.write((char*)&a.var[i].down.y, sizeof(int)); 
				f.write((char*)&a.var[i].up.x, sizeof(int)); f.write((char*)&a.var[i].up.y, sizeof(int)); 
				f.write((char*)&a.var[i].color, sizeof(COLORREF));
				f.write((char*)&a.var[i].font, sizeof(HFONT));

				int lengthtext;
				if (a.var[i].text==NULL) lengthtext=0;
				else lengthtext=wcslen(a.var[i].text);
				f.write((char*)&lengthtext, sizeof(int));
				if (a.var[i].text!=NULL) f.write((char*)a.var[i].text, sizeof(WCHAR)*(lengthtext+1)); //lengthtext phải + thêm 1
			}

			f.close();
		}
	}
	else
	{
		ofstream f;
		f.open(a.path, ios::trunc | ios::binary);

		if (a.var.size()==0) 
		{
			TCHAR k='-'; 
			f.write((char*)&k, sizeof(TCHAR)); 
			f.close(); 
			return; 
		}

		int size=a.var.size();
		f.write((char*)&size, sizeof(int));
		f.write((char*)&a.oldcolor, sizeof(COLORREF));
		for (int i=0; i<a.var.size(); ++i)
		{
			f.write((char*)&a.var[i].type, sizeof(int)); 
			f.write((char*)&a.var[i].down.x, sizeof(int)); f.write((char*)&a.var[i].down.y, sizeof(int)); 
			f.write((char*)&a.var[i].up.x, sizeof(int)); f.write((char*)&a.var[i].up.y, sizeof(int)); 
			f.write((char*)&a.var[i].color, sizeof(COLORREF));
			f.write((char*)&a.var[i].font, sizeof(HFONT));

			int lengthtext;
			if (a.var[i].text==NULL) lengthtext=0;
			else lengthtext=wcslen(a.var[i].text);
			f.write((char*)&lengthtext, sizeof(int));
			if (a.var[i].text!=NULL) f.write((char*)a.var[i].text, sizeof(WCHAR)*(lengthtext+1)); //lengthtext phải + thêm 1
		}

		f.close();
	}
}

void OpenFile(HWND hWnd)
{
	if (hEditBitmap!=NULL) 
	{
		HDC hdc=GetDC(bitmap[prev_win_id-1].hWnd);
		SetDrawText(bitmap[prev_win_id-1].hWnd, hdc, prev_win_id);
	}

	OPENFILENAME ofn;
	TCHAR  szFile[256], namefile[256];
	TCHAR  szFilter[] = TEXT("Draw (*.drw)\0* .drw\0Text (*.txt)\0 *.txt\0Data (*.dat)\0 *.dat\0");

	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;  
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;  
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFileTitle = namefile;
	ofn.nMaxFileTitle = 256;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	GetOpenFileName(&ofn);

	for (int i=0; i<bitmap.size(); ++i)
		if (bitmap[i].path!=NULL)
			if (wcscmp(bitmap[i].path,ofn.lpstrFile)==0)
			{
				TCHAR noti[280];
				wsprintf(noti, L"Can not open file %s. It is opened before!", bitmap[bitmap.size()-1].path);
				MessageBox(hWnd, noti, L"Notification", MB_OK);
				return;
			}

	ifstream f;
	f.open(szFile, ios::binary);
	if (!f.is_open()) return;

	COLORREF co;
	HFONT fo;
	int tmp;
	string text;
	ArrayShape a;

	a.path=new WCHAR[256];
	wcscpy(a.path, ofn.lpstrFile);

	TCHAR k;
	f.read((char*) &k, sizeof(TCHAR));
	if (k=='-') 
	{
		a.oldcolor=RGB(0,0,0);
		bitmap.push_back(a);
		LoadedFile=true;
		NewBitmapFileWnd(ofn.lpstrFileTitle);
		return;
	}

	f.seekg(0);
	int size;
	f.read((char*)&size, sizeof(int));
	f.read((char*)&a.oldcolor, sizeof(COLORREF));

	for (int i=0; i<size; ++i)
	{
		Shape b;
		f.read((char*)&b.type, sizeof(int));

		f.read((char*)&b.down.x, sizeof(int));	f.read((char*)&b.down.y, sizeof(int));
		f.read((char*)&b.up.x, sizeof(int));	f.read((char*)&b.up.y, sizeof(int));
		f.read((char*)&b.color, sizeof(COLORREF));
		f.read((char*)&b.font, sizeof(HFONT));

		int lengthtext;
		f.read((char*)&lengthtext, sizeof(int));
		if (lengthtext!=0) 
		{
			b.text=new WCHAR[lengthtext+1]; //lengthtext phải + thêm 1
			f.read((char*)b.text, sizeof(WCHAR)*(lengthtext+1)); //lengthtext phải + thêm 1
		}
		else b.text=NULL;

		a.var.push_back(b);
	}

	f.close();
	bitmap.push_back(a);
	LoadedFile=true;
	NewBitmapFileWnd(ofn.lpstrFileTitle);
}

void CreateToolBar(HWND hWnd)
{
	InitCommonControls();

	TBBUTTON tbButton[] =
	{
		{ STD_FILENEW,	ID_FILE_NEWTEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 }
	};

	hToolBarWnd = CreateToolbarEx(hWnd,
	WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
	ID_TOOLBAR,
	sizeof(tbButton) / sizeof(TBBUTTON),
	HINST_COMMCTRL,
	0,
	tbButton,
	sizeof(tbButton) / sizeof(TBBUTTON),
	BUTTON_WIDTH,
	BUTTON_HEIGHT,
	IMAGE_WIDTH,
	IMAGE_HEIGHT,
	sizeof(TBBUTTON));

	TBBUTTON tbButton2[] =
	{
		{ 0, ID_FILE_NEWBITMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ STD_FILEOPEN, ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE, ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_COPY, ID_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_CUT, ID_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_PASTE, ID_EDIT_PASTE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_EDIT_SELECTALL, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 2, ID_DRAW_LINE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, ID_DRAW_RECTANGLE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 4, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 5, ID_DRAW_TYPETEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 6, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 7, ID_FORMAT_CHOOSEFONT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 8, ID_FORMAT_CHOOSECOLOR, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 9, ID_TOOLBAR_VIEWHIDE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 10, ID_WINDOW_TILEVERTICAL, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 11, ID_WINDOW_TILEHORIZONTAL, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 12, ID_WINDOW_CASCADE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 13, ID_WINDOW_CLOSEALL, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	TBADDBITMAP	tbBitmap0 = { hInst, IDB_BITMAP };
	TBADDBITMAP	tbBitmap1 = { hInst, IDB_SELECTALL };
	TBADDBITMAP	tbBitmap2 = { hInst, IDB_LINE };
	TBADDBITMAP	tbBitmap3 = { hInst, IDB_RECTANGLE };
	TBADDBITMAP	tbBitmap4 = { hInst, IDB_ELLIPSE };
	TBADDBITMAP	tbBitmap5 = { hInst, IDB_TEXT };
	TBADDBITMAP	tbBitmap6 = { hInst, IDB_SELECTOBJECT };
	TBADDBITMAP	tbBitmap7 = { hInst, IDB_FONT };
	TBADDBITMAP	tbBitmap8 = { hInst, IDB_COLOR };
	TBADDBITMAP	tbBitmap9 = { hInst, IDB_VIEWHIDE };
	TBADDBITMAP	tbBitmap10 = { hInst, IDB_VERTICAL };
	TBADDBITMAP	tbBitmap11 = { hInst, IDB_HORIZONTAL };
	TBADDBITMAP	tbBitmap12 = { hInst, IDB_CASCADE };
	TBADDBITMAP	tbBitmap13 = { hInst, IDB_CLOSEALL };

	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap0);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap1);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap2);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap3);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap4);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap5);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap6);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap7);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap8);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap9);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap10);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap11);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap12);
	SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM) &tbBitmap13);

	tbButton2[0].iBitmap += idx;
	tbButton2[7].iBitmap += idx;
	tbButton2[9].iBitmap += idx;
	tbButton2[10].iBitmap += idx;
	tbButton2[11].iBitmap += idx;
	tbButton2[12].iBitmap += idx;
	tbButton2[13].iBitmap += idx;
	tbButton2[15].iBitmap += idx;
	tbButton2[16].iBitmap += idx;
	tbButton2[18].iBitmap += idx;
	tbButton2[20].iBitmap += idx;
	tbButton2[21].iBitmap += idx;
	tbButton2[22].iBitmap += idx;
	tbButton2[23].iBitmap += idx;

	SendMessage(hToolBarWnd, TB_ADDBUTTONS, sizeof(tbButton2)/sizeof(TBBUTTON), (LPARAM)&tbButton2);
}

void ViewHideToolbar(HWND hWnd, HWND hwndMDIClient, LPARAM lParam, RECT rc,bool &IsUsing)
{
	if (IsUsing)
	{
		MoveWindow(hwndMDIClient, 0, 0, rc.right, rc.bottom, TRUE);
		ShowWindow(hToolBarWnd, SW_HIDE);
		CheckMenuItem(GetMenu(hWnd), ID_TOOLBAR_VIEWHIDE, MF_UNCHECKED | MF_BYCOMMAND);
		IsUsing=false;
	}
	else
	{
		CreateToolBar(hWnd);
		MoveWindow(hwndMDIClient, 0, rc.top+28, rc.right, rc.bottom, TRUE);
		ShowWindow(hToolBarWnd, SW_SHOW);
		CheckMenuItem(GetMenu(hWnd), ID_TOOLBAR_VIEWHIDE, MF_CHECKED | MF_BYCOMMAND);
		IsUsing=true;
	}
}

void Color(HWND hWnd)
{
	CHOOSECOLOR cc; 
	COLORREF acrCustClr[16]; 
	DWORD rgbCurrent = RGB(255, 0, 0); 
									 
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd; 
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent; 
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc)) rgbCurrentColor = cc.rgbResult;

	if (hEditBitmap!=NULL) SetFocus(hEditBitmap);
}

void Font(HWND hWnd)
{
	CHOOSEFONT cf;
	LOGFONT lf;
	HFONT hfNew;

	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &lf;
	cf.iPointSize = 0;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	
	ChooseFont(&cf);
	hfNew = CreateFontIndirect(cf.lpLogFont);

	rgbCurrentColor = cf.rgbColors;
	if (hEditBitmap!=NULL)
	{
		OldFont=CurrentFont;
		SetFocus(hEditBitmap);
	}
	else OldFont=hfNew;
	CurrentFont=hfNew;
}

void NewBitmapWnd(int id)
{
	wsprintf(szBitmapTitle, L"Bitmap %d", id);
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = szBitmapWindowClass;
	mdiCreate.szTitle = szBitmapTitle;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LONG)(LPMDICREATESTRUCT)&mdiCreate);
}

LRESULT CALLBACK MDICloseProc(HWND hMDIWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hMDIWnd, 0L);
	return 1;
}

void NewBitmapFileWnd(TCHAR namefile[256])
{
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = szBitmapWindowClass;
	mdiCreate.szTitle = namefile;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LONG)(LPMDICREATESTRUCT)&mdiCreate);
}

void CreateVariableBitmap(HWND hWnd)
{
	if (!LoadedFile)
	{
		ArrayShape temp;
		temp.oldcolor=rgbCurrentColor;
		temp.path=NULL;
		temp.hWnd=hWnd;
		bitmap.push_back(temp);
	}
	else
	{
		LoadedFile=false;
		bitmap[bitmap.size()-1].hWnd=hWnd;
		TCHAR noti[280];
		wsprintf(noti, L"Open file %s successfully!", bitmap[bitmap.size()-1].path);
		MessageBox(hWnd, noti, L"Notification", MB_OK);
	}
}

void RepaintWhiteWin(HWND hwnd)
{
	HBRUSH color, draw;
	HDC hdc = GetDC(hwnd);
	RECT rc;

	GetClientRect(hwnd,&rc);
	color = CreateSolidBrush(RGB(255, 255, 255));
	draw = (HBRUSH) SelectObject(hdc, color);
	Rectangle(hdc, rc.left-1, rc.top-1, rc.right+1, rc.bottom+1);
	SelectObject(hdc, draw);

	ReleaseDC(hwnd, hdc);
}

void Repaint(HWND hWnd, HDC hdc, int winid)
{
	if (winid==-1) return;
	ArrayShape a = bitmap[winid-1];
	if (a.var.size()==0) return;

	Shape b;
	HPEN Pen, OldPen;
	hdc=GetDC(hWnd);
	HBRUSH oldBr = (HBRUSH)SelectObject(hdc, GetStockObject(NULL_BRUSH));
	RECT rc;
	int lx, ly;

	for (int i=0; i<a.var.size(); ++i)
	{
		b=a.var[i];
		if (b.type!=TYPETEXT) Pen = CreatePen(PS_SOLID, 3, b.color);
		else Pen = CreatePen(PS_DASHDOT, 1, b.color);
		OldPen = (HPEN)SelectObject(hdc, Pen);
		
		switch (b.type)
		{
			case RECTANGLE:
				MoveToEx(hdc, b.down.x, b.down.y, NULL);
				Rectangle(hdc, b.down.x, b.down.y, b.up.x, b.up.y);
				break;
			case ELLIPSE:
				MoveToEx(hdc, b.down.x, b.down.y, NULL);
				Ellipse(hdc, b.down.x, b.down.y, b.up.x, b.up.y);
				break;
			case LINE:
				MoveToEx(hdc, b.down.x, b.down.y, NULL);
				LineTo(hdc, b.up.x, b.up.y);
				break;
			case TYPETEXT:
				MoveToEx(hdc, b.down.x, b.down.y, NULL);
				Rectangle(hdc, b.down.x, b.down.y, b.up.x, b.up.y);

				lx = (b.down.x + 5) + abs(b.down.x-b.up.x)-10;
				ly = (b.down.y + 5) + abs(b.down.y-b.up.y)-10;
				SetRect(&rc, b.down.x+5, b.down.y+5, lx, ly);
				SelectObject(hdc, b.font);
				SetTextColor(hdc, b.color);
			
				DrawText(hdc, b.text, lstrlen(b.text), &rc, DT_LEFT | DT_EDITCONTROL | DT_WORDBREAK);
				break;
			default: break;
		}
	}

	SelectObject(hdc, oldBr);
	SelectObject(hdc, OldPen);
	ReleaseDC(hWnd, hdc);
}

void RepaintRecForWinText(HWND hWnd, HDC hdc, int winid)
{
	HPEN Pen, OldPen;
	hdc=GetDC(hWnd);
	HBRUSH oldBr = (HBRUSH)SelectObject(hdc, GetStockObject(NULL_BRUSH));

	int size=bitmap[winid-1].var.size();

	Pen = CreatePen(PS_DASHDOT, 1, bitmap[winid-1].var[size-1].color);
	OldPen = (HPEN)SelectObject(hdc, Pen);

	MoveToEx(hdc, bitmap[winid-1].var[size-1].down.x,  bitmap[winid-1].var[size-1].down.y, NULL);
	Rectangle(hdc,  bitmap[winid-1].var[size-1].down.x,  bitmap[winid-1].var[size-1].down.y,
		bitmap[winid-1].var[size-1].up.x,  bitmap[winid-1].var[size-1].up.y);

	SelectObject(hdc, oldBr);
	SelectObject(hdc, OldPen);
	ReleaseDC(hWnd, hdc);
}

void SwapPointUpPointDown(point2d &down, point2d &up)
{
	point2d tempdown, tempup;

	if (up.x>down.x)
	{
		if (up.y>=down.y) return;
		else
		{
			tempdown.x=down.x;
			tempdown.y=up.y;
			tempup.x=up.x;
			tempup.y=down.y;
		}
	}
	else
	{
		if (up.y<=down.y)
		{
			swap(down,up);
			return;
		}
		else 
		{
			tempdown.x=up.x;
			tempdown.y=down.y;
			tempup.x=down.x;
			tempup.y=up.y;
		}
	}

	down.x=tempdown.x;
	down.y=tempdown.y;
	up.x=tempup.x;
	up.y=tempup.y;
}

void InitWinTypeText(HWND hWnd, Shape a)
{
	hEditBitmap=CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | ES_MULTILINE, 
		a.down.x+5, a.down.y+5, abs(a.down.x-a.up.x)-10, abs(a.down.y-a.up.y)-10, hWnd, NULL, hInst, 0L);

	SetFocus(hEditBitmap);
}

void SetDrawText(HWND hWnd, HDC hdc, int winid)
{
	RECT rc;
	hdc=GetDC(hWnd);
	int sizeshape=bitmap[winid-1].var.size();
	bitmap[winid-1].var[sizeshape-1].font=CurrentFont;
	int lengthtext=GetWindowTextLength(hEditBitmap)+1;
	bitmap[winid-1].var[sizeshape-1].text = new WCHAR[lengthtext];
	GetWindowText(hEditBitmap, bitmap[winid-1].var[sizeshape-1].text, GetWindowTextLength(hEditBitmap)+1);

	GetClientRect(hEditBitmap, &rc);
	MapWindowPoints(hEditBitmap, GetParent(hEditBitmap), (LPPOINT)&rc, 2);

	DestroyWindow(hEditBitmap);
	hEditBitmap=NULL;
		
	SelectObject(hdc, bitmap[winid-1].var[sizeshape-1].font);
	bitmap[winid-1].var[sizeshape-1].color=rgbCurrentColor;
	SetTextColor(hdc, bitmap[winid-1].var[sizeshape-1].color);
			
	DrawText(hdc, bitmap[winid-1].var[sizeshape-1].text, lstrlen(bitmap[winid-1].var[sizeshape-1].text), 
		&rc, DT_LEFT | DT_EDITCONTROL | DT_WORDBREAK);
				
	ReleaseDC(hWnd,hdc);
	CurrentFont=OldFont;
}

bool CheckLineAndMouse(point2d coor, Shape b)
{
	int xdown=b.down.x, ydown=b.down.y, xup=b.up.x, yup=b.up.y;
	
	if (coor.x>=xdown && coor.x<=xup)
		if ((coor.y>=ydown && coor.y<=yup) || (coor.y>=yup && coor.y<=ydown)) 
		{
			/* KIẾM GIÁ TRỊ a VÀ b thông qua 2 điểm.
			ax1 + b = y1
			ax2 + b = y2

			b=y1-ax1
			a=(y2-y1)/(x2-x1)
			
			Xem m là a, xem n là b*/

			double m, n;
			m=(b.up.y-b.down.y)*1.0/(b.up.x-b.down.x);
			n=b.down.y-m*b.down.x;
			double value=m*coor.x + n - coor.y;

			if (value>-5 && value<5) return true;
		}
		
	return false;
}

bool CheckRectangleAndMouse(point2d coor, Shape b)
{
	int xdown=b.down.x, ydown=b.down.y, xup=b.up.x, yup=b.up.y;

	if (coor.x>=xdown && coor.x<=xup)
		if ((coor.y>=ydown && coor.y<=yup) || (coor.y>=yup && coor.y<=ydown)) 
			return true;

	return false;
}

void MoveShape(HWND hWnd, int pos, int winid, point2d oldcoor, point2d newcoor)
{
	Shape b = bitmap[winid-1].var[pos];

	HDC hdc = GetDC(hWnd);
	HBRUSH oldBr = (HBRUSH)SelectObject(hdc, GetStockObject(NULL_BRUSH));
	HPEN Pen, OldPen;
	RECT rc;
	int lx, ly;

	//Xóa trắng khi di chuyển object
	RepaintWhiteWin(hWnd);
	//-------------------------------

	int distance_x = newcoor.x - oldcoor.x;
	int distance_y = newcoor.y - oldcoor.y;

	bitmap[winid-1].var[pos].down.x += distance_x;
	bitmap[winid-1].var[pos].down.y += distance_y;
	bitmap[winid-1].var[pos].up.x	+= distance_x;
	bitmap[winid-1].var[pos].up.y	+= distance_y;
	//-------------------------

	b = bitmap[winid-1].var[pos];

	//Vẽ hình mới
	//------------
	if (b.type!=TYPETEXT) Pen = CreatePen(PS_SOLID, 3, b.color);
	else Pen = CreatePen(PS_DASHDOT, 1, b.color);
	OldPen = (HPEN)SelectObject(hdc, Pen);
	
	switch (bitmap[winid-1].var[pos].type)
	{
		case RECTANGLE:
			MoveToEx(hdc, b.down.x, b.down.y, NULL);
			Rectangle(hdc, b.down.x, b.down.y, b.up.x, b.up.y);
			break;
		case ELLIPSE:
			MoveToEx(hdc, b.down.x, b.down.y, NULL);
			Ellipse(hdc, b.down.x, b.down.y, b.up.x, b.up.y);
			break;
		case LINE:
			MoveToEx(hdc, b.down.x, b.down.y, NULL);
			LineTo(hdc, b.up.x, b.up.y);
			break;
		case TYPETEXT:
			MoveToEx(hdc, b.down.x, b.down.y, NULL);
			Rectangle(hdc, b.down.x, b.down.y, b.up.x, b.up.y);

			lx = (b.down.x + 5) + abs(b.down.x-b.up.x)-10;
			ly = (b.down.y + 5) + abs(b.down.y-b.up.y)-10;
			SetRect(&rc, b.down.x+5, b.down.y+5, lx, ly);
			SelectObject(hdc, b.font);
			SetTextColor(hdc, b.color);
			
			DrawText(hdc, b.text, lstrlen(b.text), &rc, DT_LEFT | DT_EDITCONTROL | DT_WORDBREAK);
			break;
		default: break;
	}

	SelectObject(hdc, oldBr);
	SelectObject(hdc, OldPen);
	ReleaseDC(hWnd, hdc);
}

void TypeResize(point2d coor, int pos, int winid, int &typeresize)
{
	Shape a = bitmap[winid-1].var[pos];
	point2d k; 
	switch(a.type)
	{
		case RECTANGLE: case TYPETEXT: case ELLIPSE:
			if (coor.x>=a.down.x-3 && coor.x<=a.down.x+5 && coor.y>=a.down.y-3 && coor.y<=a.down.y+5) {typeresize=0; return;}
			k.x=a.up.x;
			k.y=a.down.y;
			if (coor.x>=k.x-5 && coor.x<=k.x+3 && coor.y>=k.y-3 && coor.y<=k.y+5) {typeresize=1; return;}
			if (coor.x>=a.up.x-5 && coor.x<=a.up.x+3 && coor.y>=a.up.y-5 && coor.y<=a.up.y+3) {typeresize=2; return;}
			k.x=a.down.x;
			k.y=a.up.y;
			if (coor.x>=k.x-3 && coor.x<=k.x+5 && coor.y>=k.y-5 && coor.y<=k.y+3) {typeresize=3; return;}
			break;
		case LINE:
			if (coor.x>=a.down.x-4 && coor.x<=a.down.x+4 && coor.y>=a.down.y-4 && coor.y<=a.down.y+4) {typeresize=4; return;}
			if (coor.x>=a.up.x-4 && coor.x<=a.up.x+4 && coor.y>=a.up.y-4 && coor.y<=a.up.y+4) {typeresize=5; return;}
			break;
		default: break;
	}
}

int GetCurrentShape(point2d coor, HWND hWnd, int winid)
{
	if (winid==-1) return -1;
	ArrayShape a = bitmap[winid-1];
	int size=a.var.size();
	if (size==0) return -1;

	bool check=false;
	for (int i=size-1; i>=0; --i)
	{ 
		Shape b = a.var[i];
		switch (b.type)
		{ 
			case LINE:
				check=CheckLineAndMouse(coor, b);
				break;
			case RECTANGLE: case TYPETEXT: case ELLIPSE:
				check=CheckRectangleAndMouse(coor, b);
				break;
			default: break;
		}
		if (check) return i;
	}

	return -1;
}

void EllipsePointsForSelect(HWND hWnd, int type, Shape b)
{
	HDC dc = GetDC(hWnd);
	point2d k;

	switch (type)
	{
		case RECTANGLE: case TYPETEXT: case ELLIPSE:
			Ellipse(dc, b.down.x-3, b.down.y-3, b.down.x+5, b.down.y+5);
			k.x=b.up.x;
			k.y=b.down.y;
			Ellipse(dc, k.x-5, k.y-3, k.x+3, k.y+5);
			Ellipse(dc, b.up.x-5, b.up.y-5, b.up.x+3, b.up.y+3);
			k.x=b.down.x;
			k.y=b.up.y;
			Ellipse(dc, k.x-3, k.y-5, k.x+5, k.y+3);
			break;
		case LINE:
			Ellipse(dc, b.down.x-4, b.down.y-4, b.down.x+4, b.down.y+4);
			Ellipse(dc, b.up.x-4, b.up.y-4, b.up.x+4, b.up.y+4);
			break;
		default: break;
	}

	ReleaseDC(hWnd, dc);
}

void ResizeShape(HWND hWnd, int pos, int winid, int typeresize, point2d newcoor)
{
	HDC hdc = GetDC(hWnd);
	HPEN hPen, hOldPen;
	Shape a = bitmap[winid-1].var[pos];
	int distance_x, distance_y;

	//Xóa trắng khi di chuyển object
	RepaintWhiteWin(hWnd);
	//-------------------------------

	if (a.type!=TYPETEXT) hPen = CreatePen(PS_SOLID, 3, a.color);
	else hPen = CreatePen(PS_DASHDOT, 1, a.color);
	hOldPen = (HPEN)SelectObject(hdc, hPen);

	switch (a.type)
	{
		case RECTANGLE: case TYPETEXT: case ELLIPSE:
			switch(typeresize)
			{
				case 0:
					if (newcoor.x>a.up.x-8 || newcoor.y>a.up.y-8) break;
					distance_x=newcoor.x-a.down.x;
					distance_y=newcoor.y-a.down.y;
					bitmap[winid-1].var[pos].down.x+=distance_x;
					bitmap[winid-1].var[pos].down.y+=distance_y;
					break;
				case 1:
					if (newcoor.x<a.down.x+8 || newcoor.y>a.up.y-8) break;
					distance_x=newcoor.x-a.up.x;
					distance_y=newcoor.y-a.down.y;
					bitmap[winid-1].var[pos].up.x+=distance_x;
					bitmap[winid-1].var[pos].down.y+=distance_y;
					break;
				case 2:
					if (newcoor.x<a.down.x+8 || newcoor.y<a.down.y+8) break;
					distance_x=newcoor.x-a.up.x;
					distance_y=newcoor.y-a.up.y;
					bitmap[winid-1].var[pos].up.x+=distance_x;
					bitmap[winid-1].var[pos].up.y+=distance_y;
					break;
				case 3:
					if (newcoor.x>a.up.x-8 || newcoor.y<a.down.y+8) break;
					distance_x=newcoor.x-a.down.x;
					distance_y=newcoor.y-a.up.y;
					bitmap[winid-1].var[pos].down.x+=distance_x;
					bitmap[winid-1].var[pos].up.y+=distance_y;
					break;
			}
			a=bitmap[winid-1].var[pos];
			if (a.type==ELLIPSE) Ellipse(hdc, a.down.x, a.down.y, a.up.x, a.up.y);
			else Rectangle(hdc, a.down.x, a.down.y, a.up.x, a.up.y);
			break;
		case LINE:
			if (typeresize==4)
			{
				MoveToEx(hdc, a.up.x, a.up.y, NULL);
				distance_x=newcoor.x-a.down.x;
				distance_y=newcoor.y-a.down.y;
				bitmap[winid-1].var[pos].down.x+=distance_x;
				bitmap[winid-1].var[pos].down.y+=distance_y;
				a=bitmap[winid-1].var[pos];
				LineTo(hdc, a.down.x, a.down.y);
			}
			else
			{
				MoveToEx(hdc, a.down.x, a.down.y, NULL);
				distance_x=newcoor.x-a.up.x;
				distance_y=newcoor.y-a.up.y;
				bitmap[winid-1].var[pos].up.x+=distance_x;
				bitmap[winid-1].var[pos].up.y+=distance_y;
				a=bitmap[winid-1].var[pos];
				LineTo(hdc, a.up.x, a.up.y);
			}
			break;
		default:
			break;
	}

	SelectObject(hdc, hOldPen);
	ReleaseDC(hWnd, hdc);
}

LRESULT CALLBACK BitmapChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc = GetDC(hWnd);
	static int x1, x2, y1, y2;
	HPEN hPen, hOldPen;
	static int winid=-1;
	int sizeshape=-1;
	static Shape oneforadd;
	//static int pos=-1;
	static int typeresize=-1;

	TCHAR mes[30]; 

	switch (message)
	{
		case WM_ENTERSIZEMOVE:
			if (hEditBitmap!=NULL) SetDrawText(hWnd, hdc, winid);
			break;
		case WM_CREATE:
			CreateVariableBitmap(hWnd);
			break;
		case WM_MDIACTIVATE:
			if (hWnd == (HWND) lParam)
			{	
				for (int i=0; i<bitmap.size(); ++i) 
					if (bitmap[i].hWnd==(HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL))
					{
						winid=i+1; 
						break;
					}
				
				if (hEditBitmap!=NULL) SetDrawText(hWnd, hdc, prev_win_id);

				pos=-1;
				prev_win_id=winid;
				
				RepaintWhiteWin(hWnd);
				Repaint(hWnd,hdc,winid);
				
				rgbCurrentColor=bitmap[winid-1].oldcolor;
			}
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		case WM_CLOSE:
			pos=-1;
			if (hEditBitmap!=NULL) SetDrawText(hWnd, hdc, prev_win_id);
			if (bitmap[winid-1].path!=NULL) { delete []bitmap[winid-1].path; bitmap[winid-1].path=NULL; }
			for (int i=0; i<bitmap[winid-1].var.size(); ++i)
				if (bitmap[winid-1].var[i].text!=NULL)
				{
					delete []bitmap[winid-1].var[i].text;
					bitmap[winid-1].var[i].text=NULL;
				}
			bitmap.erase(bitmap.begin() + winid - 1);
			break;
		case WM_PAINT:
			PAINTSTRUCT ps;
			hdc = BeginPaint(hWnd, &ps);
			//Ham ve lai screen
			Repaint(hWnd,hdc,winid);
			//-------------------
			EndPaint(hWnd, &ps);
			return 0;
		case WM_SETCURSOR:
			if (LOWORD(lParam)==HTCLIENT)
			{
				HCURSOR cursor;
				if (TypeShape==SELECTOBJECT) cursor = LoadCursor(NULL,IDC_ARROW);
				else cursor = LoadCursor(NULL,IDC_CROSS);
				SetCursor(cursor);
				return TRUE;
			}
			break;
		case WM_CTLCOLOREDIT:
			hdc = (HDC) wParam;
			sizeshape=bitmap[winid-1].var.size();
			bitmap[winid-1].var[sizeshape-1].color=rgbCurrentColor;
			RepaintRecForWinText(hWnd, hdc, winid);
			SetTextColor(hdc, rgbCurrentColor);
			return NULL;
		case WM_LBUTTONDOWN:
			x1 = x2 = LOWORD(lParam);
			y1 = y2 = HIWORD(lParam);
			
			if (hEditBitmap!=NULL)
			{
				SetDrawText(hWnd, hdc, winid);
				rgbCurrentColor=bitmap[winid-1].oldcolor;
			}

			if (TypeShape==SELECTOBJECT)
			{
				point2d c; c.x = x1; c.y = y1;
				pos = GetCurrentShape(c,hWnd,winid);
				if (pos!=-1) 
				{
					RepaintWhiteWin(hWnd);
					Repaint(hWnd, hdc, winid);
					TypeResize(c,pos,winid,typeresize);
					EllipsePointsForSelect(hWnd, bitmap[winid-1].var[pos].type, bitmap[winid-1].var[pos]);					
				}
				break;
			}

			oneforadd.color=rgbCurrentColor;
			oneforadd.type=TypeShape;
			oneforadd.down.x=x1;
			oneforadd.down.y=y1;
			break;
		case WM_LBUTTONUP:
			if (TypeShape==SELECTOBJECT) 
			{
				if (pos!=-1) 
				{
					if (typeresize==4 || typeresize==5) 
						if (bitmap[winid-1].var[pos].down.x>bitmap[winid-1].var[pos].up.x 
							|| bitmap[winid-1].var[pos].up.x<bitmap[winid-1].var[pos].down.x) 
							swap(bitmap[winid-1].var[pos].down, bitmap[winid-1].var[pos].up);
					Repaint(hWnd, hdc, winid);
					EllipsePointsForSelect(hWnd, bitmap[winid-1].var[pos].type, bitmap[winid-1].var[pos]);
					typeresize=-1;
				}
				break;
			}

			oneforadd.up.x=LOWORD(lParam);
			oneforadd.up.y=HIWORD(lParam);

			if (x1!=x2 && y1!=y2) 
			{	
				if (TypeShape!=LINE) SwapPointUpPointDown(oneforadd.down, oneforadd.up);
				else if (oneforadd.down.x>oneforadd.up.x) swap(oneforadd.down, oneforadd.up);
				
				if (TypeShape==TYPETEXT)
				{
					Repaint(hWnd,hdc,winid);
					InitWinTypeText(hWnd,oneforadd);
				}
				
				oneforadd.text=NULL;
				bitmap[winid-1].oldcolor=oneforadd.color;
				bitmap[winid-1].var.push_back(oneforadd);
			}
			break;
		case WM_MOUSEMOVE:
			if (!(wParam & MK_LBUTTON)) return 0;
			
			if (TypeShape==SELECTOBJECT && pos!=-1)
			{
				x2=LOWORD(lParam);
				y2=HIWORD(lParam);

				point2d p1, p2;
				p1.x=x1; p1.y=y1;
				p2.x=x2; p2.y=y2;
				
				if (typeresize==-1)
				{
					MoveShape(hWnd, pos, winid, p1, p2);
					x1=x2; y1=y2;
				}
				else
				{
					ResizeShape(hWnd, pos, winid, typeresize, p2);
				}

				break;
			}

			//----------------------------------------------

			if (TypeShape!=TYPETEXT) hPen = CreatePen(PS_SOLID, 3, rgbCurrentColor);
			else hPen = CreatePen(PS_DASHDOT, 1, rgbCurrentColor);
			hOldPen = (HPEN)SelectObject(hdc, hPen);

			SetROP2(hdc, R2_NOTXORPEN);
			MoveToEx(hdc,x1,y1,NULL);
			switch (TypeShape)
			{
				case RECTANGLE:
					Rectangle(hdc, x1, y1, x2, y2);
					break;
				case ELLIPSE:
					Ellipse(hdc, x1, y1, x2, y2);
					break;
				case LINE:
					LineTo(hdc, x2, y2);
					break;
				case TYPETEXT:
					Rectangle(hdc, x1, y1, x2, y2);
					break;
				default:
					break;
			}

			x2=LOWORD(lParam);
			y2=HIWORD(lParam);
			MoveToEx(hdc,x1,y1,NULL);
			switch (TypeShape)
			{
				case RECTANGLE:
					Rectangle(hdc, x1, y1, x2, y2);
					break;
				case ELLIPSE:
					Ellipse(hdc, x1, y1, x2, y2);
					break;
				case LINE:
					LineTo(hdc, x2, y2);
					break;
				case TYPETEXT:
					Rectangle(hdc, x1, y1, x2, y2);
					break;
				default:
					break;
			}

			SelectObject(hdc, hOldPen);
			ReleaseDC(hWnd, hdc);
			break;
		default:
            return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}

void DelObject(HWND hWnd)
{
	//--------------------------
	if (pos==-1) return;
	int winid=-1;
	HWND hw=(HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
	for (int i=0; i<bitmap.size(); ++i) 
		if (bitmap[i].hWnd==hw)
		{
			winid=i+1;
			break;
		}
	if (winid==-1) return;
	if (bitmap[winid-1].var.size()==0) return;
	//--------------------------
	bitmap[winid-1].var.erase(bitmap[winid-1].var.begin() + pos);
	HDC hdc=GetDC(hw);
	RepaintWhiteWin(hw);
	Repaint(hw, hdc, winid);
	pos=-1;
}

void ClipboardCut_Copy(HWND hWnd, bool cutcopy) //cutcopy=true => cut | cutcopy=false => copy
{
	//--------------------------
	if (pos==-1) return;
	int winid=-1;
	HWND hw=(HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
	for (int i=0; i<bitmap.size(); ++i) 
		if (bitmap[i].hWnd==hw)
		{
			winid=i+1;
			break;
		}

	if (winid==-1) return;
	if (bitmap[winid-1].var.size()==0) return;
	//--------------------------

	int ShapeFormatID = RegisterClipboardFormat(L"Shape_Object");
	HGLOBAL hgb = GlobalAlloc(GHND, sizeof(Shape));
	Shape* p = (Shape*) GlobalLock(hgb);

	p->color = bitmap[winid-1].var[pos].color;
	p->down.x = bitmap[winid-1].var[pos].down.x;
	p->down.y = bitmap[winid-1].var[pos].down.y;
	p->up.x = bitmap[winid-1].var[pos].up.x;
	p->up.y = bitmap[winid-1].var[pos].up.y;
	p->font = bitmap[winid-1].var[pos].font;
	p->type = bitmap[winid-1].var[pos].type;
	if (bitmap[winid-1].var[pos].text!=NULL)
	{
		p->text=new WCHAR[wcslen(bitmap[winid-1].var[pos].text)+1];
		wcscpy(p->text, bitmap[winid-1].var[pos].text);
	}
	else p->text=NULL;
	
	GlobalUnlock(hgb);
	if (OpenClipboard(hw))
	{
		EmptyClipboard();
		SetClipboardData(ShapeFormatID, hgb);
		CloseClipboard();
	}

	//--------------------------
	if (cutcopy) //cutcopy=true => cut
	{
		bitmap[winid-1].var.erase(bitmap[winid-1].var.begin() + pos);
		HDC hdc=GetDC(hw);
		RepaintWhiteWin(hw);
		Repaint(hw, hdc, winid);
		pos=-1;
	}
}

void ClipboardPaste(HWND hWnd)
{
	//--------------------------
	int winid=-1;
	HWND hw=(HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
	for (int i=0; i<bitmap.size(); ++i) 
		if (bitmap[i].hWnd==hw)
		{
			winid=i+1;
			break;
		}
	
	if (winid==-1) return; 
	//--------------------------

	if (OpenClipboard(hw))
	{
		HDC hdc = GetDC(hw);
		int ShapeFormatID = RegisterClipboardFormat(L"Shape_Object");
		HGLOBAL hgb = GetClipboardData(ShapeFormatID);
		
		if (hgb)
		{ 
			Shape* p = (Shape*) GlobalLock(hgb);
			Shape des;
			if (p->text!=NULL) 
			{
				des.text = new WCHAR[wcslen(p->text)+1];
				wcscpy(des.text, p->text);
				/*if (wcslen(p->text)!=0)
				{
					des.text = new WCHAR[wcslen(p->text)+1];
					wcscpy(des.text, p->text);
				}
				else 
				{
					CloseClipboard();
					return;
				}*/
			}
			else des.text=NULL;
					
			des.color=p->color;
			des.down.x=p->down.x;
			des.down.y=p->down.y;
			des.up.x=p->up.x;
			des.up.y=p->up.y;
			des.font=p->font;
			des.type=p->type; 
			GlobalUnlock(hgb);
			
			bitmap[winid-1].var.push_back(des); 
			pos=bitmap[winid-1].var.size()-1;
			
			RepaintWhiteWin(hw);
			Repaint(hw, hdc, winid);
			EllipsePointsForSelect(hw, bitmap[winid-1].var[pos].type,  bitmap[winid-1].var[pos]); 
			ReleaseDC(hw, hdc);
		}
		else
		{
			HANDLE handle = GetClipboardData(CF_UNICODETEXT);
			if (handle)
			{
				Shape des;
				WCHAR *src = (WCHAR*) GlobalLock(handle);
				int length = wcslen(src)+1;
				des.text = new WCHAR[length];
				wcscpy(des.text,src);
				GlobalUnlock(handle);

				des.down.x=0;
				des.down.y=0;
				des.type=TYPETEXT;
				des.up.x=150;
				des.up.y=80;
				des.font=CurrentFont;
				des.color=rgbCurrentColor;

				bitmap[winid-1].var.push_back(des);
				pos=bitmap[winid-1].var.size()-1;
				Repaint(hw, hdc, winid);
				EllipsePointsForSelect(hw, bitmap[winid-1].var[pos].type,  bitmap[winid-1].var[pos]);
				ReleaseDC(hw, hdc);
			}
		}
		CloseClipboard();
	}
}



