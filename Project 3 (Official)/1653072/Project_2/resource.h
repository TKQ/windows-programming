//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Project_2.rc
//
#define IDC_MYICON                      2
#define IDD_PROJECT_2_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_PROJECT_2                   107
#define IDI_SMALL                       108
#define IDC_PROJECT_2                   109
#define IDS_BITMAP_TITLE                110
#define IDC_BITMAP_CLASS                111
#define IDS_TEXT_TITLE                  112
#define IDC_TEXT_CLASS                  113
#define IDR_MAINFRAME                   128
#define IDI_BMP_WIN                     129
#define IDI_MYPAINT                     130
#define IDI_ICON3                       131
#define IDI_TEXT_WIN                    131
#define IDB_BITMAP                      132
#define IDB_TEXT                        133
#define IDB_SELECTALL                   134
#define IDB_CASCADE                     135
#define IDB_CLOSEALL                    136
#define IDB_COLOR                       137
#define IDB_ELLIPSE                     138
#define IDB_FONT                        139
#define IDB_HORIZONTAL                  140
#define IDB_LINE                        141
#define IDB_RECTANGLE                   142
#define IDB_SELECTOBJECT                143
#define IDB_VERTICAL                    144
#define IDB_VIEWHIDE                    145
#define ID_FILE_NEWTEXT                 32771
#define ID_FILE_NEWBITMAP               32772
#define ID_FILE_OPEN                    32773
#define ID_FILE_SAVE                    32774
#define ID_EDIT_CUT                     32775
#define ID_EDIT_COPY                    32776
#define ID_EDIT_PASTE                   32777
#define ID_EDIT_SELECTALL               32778
#define ID_DRAW_LINE                    32779
#define ID_DRAW_RECTANGLE               32780
#define ID_DRAW_ELLIPSE                 32781
#define ID_DRAW_TYPETEXT                32782
#define ID_DRAW_SELECTOBJECT            32783
#define ID_DRAW_FORMAT                  32784
#define ID_FORMAT_CHOOSECOLOR           32785
#define ID_FORMAT_CHOOSEFONT            32786
#define ID_TOOLBAR_VIEW                 32787
#define ID_TOOLBAR_VIEWHIDE             32788
#define ID_WINDOW_TIDEVERTICAL          32789
#define ID_WINDOW_TILEHORIZONTAL        32790
#define ID_WINDOW_TILEVERTICAL          32791
#define ID_WINDOW_CASCADE               32792
#define ID_WINDOW_CLOSEALL              32793
#define ID_EDIT_DELETEOBJECT            32816
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         32818
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
