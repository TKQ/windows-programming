#pragma once

#include "resource.h"
#include <vector>
#include <iostream>
using namespace std;
#include <string>
#include <math.h>
#include <fstream>

#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")
#include <commdlg.h>

#define MAX_LOADSTRING 100

//Khai bao bien de phuc vu cho viec tao TOOLBAR
HWND  hToolBarWnd;
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH   0
#define BUTTON_HEIGHT   0
#define ID_TOOLBAR		1000
#define TOOL_TIP_MAX_LEN   32

//Khai bao HWND de giu HWND WINMAIN de co the xu ly cac accelerator
HWND hWndWin;

//Khai bao HMENU de hien/an menu item
HMENU hMenu;

//Tao child window moi theo kieu BitMap
TCHAR szBitmapTitle[MAX_LOADSTRING];            
TCHAR szBitmapWindowClass[MAX_LOADSTRING];
LRESULT CALLBACK BitmapChildWndProc(HWND, UINT, WPARAM, LPARAM);

//Quan ly cac child window
LRESULT CALLBACK MDICloseProc(HWND hMDIWnd, LPARAM lParam);
HWND hwndMDIClient;

//Khai bao bien de phuc vu cho CHOOSE_FONT va CHOOSE_COLOR
COLORREF rgbCurrentColor = RGB(0, 0, 0);

//-----------------Cac ham quan trong-----------------
void NewBitmapWnd(int id);
void Color(HWND hWnd);
void Font(HWND hWnd);
void CreateToolBar(HWND hWnd);
void ViewHideToolbar(HWND hWnd, HWND hwndMDIClient, LPARAM lParam,RECT rc,bool &IsUsing);
int CheckIDFile();
void SaveData(HWND hWnd);
void OpenFile(HWND hWnd);


//-----------------Xu ly BitmapWinProc-----------------
#define LINE			0
#define RECTANGLE		1
#define ELLIPSE			2
#define TYPETEXT		3
#define SELECTOBJECT	4

struct point2d
{
	int x;
	int y;
};

struct Shape
{
	int type;
	point2d up;
	point2d down;
	COLORREF color;
	HFONT font;
	WCHAR* text;
};

struct ArrayShape
{
	vector<Shape> var;
	HWND hWnd;
	WCHAR* path;
	COLORREF oldcolor;
};

int TypeShape; //TypeShape dua tren define ve line, rectangle, ellipse, typetext.
bool LoadedFile=false;
HFONT CurrentFont=(HFONT) GetStockObject(SYSTEM_FONT);
HFONT OldFont=(HFONT) GetStockObject(SYSTEM_FONT);
int prev_win_id=-1;
HWND hEditBitmap;
vector<ArrayShape> bitmap;
int pos=-1;

void NewBitmapFileWnd(TCHAR namefile[256]);
void CreateVariableBitmap(HWND hWnd);
void RepaintWhiteWin(HWND hwnd);
void Repaint(HWND hWnd, HDC hdc, int winid);
void RepaintRecForWinText(HWND hWnd, HDC hdc, int winid);
void SwapPointUpPointDown(point2d &down, point2d &up);
void SetDrawText(HWND hWnd, HDC hdc, int winid);
void InitWinTypeText(HWND hWnd, Shape a);
int GetCurrentShape(point2d coor, HWND hWnd, int winid);
bool CheckLineAndMouse(point2d coor, Shape b);
bool CheckRectangleAndMouse(point2d coor, Shape b);
void EllipsePointsForSelect(HWND hWnd, int type, Shape b);
void RepaintRecForWinText(HWND hWnd, int winid, int pos);
void MoveShape(HWND hWnd, int pos, int winid, point2d oldcoor, point2d newcoor);
void TypeResize(point2d coor, int pos, int winid, int &typeresize);
void ResizeShape(HWND hWnd, int pos, int winid, int typeresize, point2d newcoor);
void DelObject(HWND hWnd);
void ClipboardCut_Copy(HWND hWnd, bool cutcopy); //cutcopy=true => cut | cutcopy=false => copy
void ClipboardPaste(HWND hWnd);




//---------------------------------
struct SO //Shape_Object for copy-cut
{
	int type;
	point2d up;
	point2d down;
	COLORREF color;
	HFONT font;
	WCHAR text[5000];
};




