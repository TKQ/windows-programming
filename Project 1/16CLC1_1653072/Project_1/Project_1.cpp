// Project_1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Project_1.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

//---------------------------------------QUOCTRAN---------------------------------------
#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")
#include <commdlg.h>

//Tao child window moi theo kieu BitMap
TCHAR szBitmapTitle[MAX_LOADSTRING];            
TCHAR szBitmapWindowClass[MAX_LOADSTRING];
LRESULT CALLBACK BitmapChildWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK MDICloseProc(HWND hMDIWnd, LPARAM lParam);
HWND hwndMDIClient;

//Khai bao bien de phuc vu cho CHOOSE_FONT va CHOOSE_COLOR
COLORREF rgbCurrentColor = RGB(0, 0, 0);

//Khai bao bien de phuc vu cho viec tao TOOLBAR
HWND  hToolBarWnd;
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH   0
#define BUTTON_HEIGHT   0
#define ID_TOOLBAR		1000
#define TOOL_TIP_MAX_LEN   32

//---------------------------------------QUOCTRAN---------------------------------------

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PROJECT_1, szWindowClass, MAX_LOADSTRING);
	LoadString(hInstance, IDS_BITMAP_TITLE, szBitmapTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_BITMAP_CLASS, szBitmapWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT_1));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	ATOM main, bitmap;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT_1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_PROJECT_1);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	main = RegisterClassEx(&wcex);

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= BitmapChildWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT_1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szBitmapWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	bitmap = RegisterClassEx(&wcex);

	return (main && bitmap);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, CW_USEDEFAULT, 600, 600, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	//-------------QUOCTRAN-------------
	static int IDBitmap = 0;
	TCHAR msg[20];
	static bool line=false, rec=false, ell=false, text=false, select=false, toolbar=false;
	RECT rc;
	//-------------QUOCTRAN-------------

	switch (message)
	{
		case WM_CREATE:
			//Khoi tao CLIENT WINDOW trong FRAME WINDOW de san sang chua cac CHILD WINDOW khac
			CLIENTCREATESTRUCT ccs;
			ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 3);
			ccs.idFirstChild = 50000;
			hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, 
				WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL ,
				0, 0, 0, 0, hWnd, (HMENU) NULL, hInst, (LPSTR)&ccs);
			ShowWindow(hwndMDIClient, SW_SHOW);
			
			//Line duoc check mac dinh
			line=true;
			CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND); 
			break;
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
				case ID_FILE_NEW:
					IDBitmap+=1;
					NewBitmapWnd(IDBitmap);
					break;
				case ID_WINDOW_TIDEVERTICAL:
					SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0L);
					break;
				case ID_WINDOW_TIDEHORIZONTAL:
					SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_HORIZONTAL, 0L);
					break;
				case ID_WINDOW_CASCADE:
					SendMessage(hwndMDIClient, WM_MDICASCADE, MDITILE_SKIPDISABLED, 0L);
					break;
				case ID_WINDOW_CLOSEALL:
					IDBitmap=0;
					EnumChildWindows(hwndMDIClient, (WNDENUMPROC) MDICloseProc, 0L);
					break;
				case ID_FILE_OPEN:
					wsprintf(msg,L"You chose OPEN!");
					MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
					break;
				case ID_FILE_SAVE:
					wsprintf(msg,L"You chose SAVE!");
					MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
					break;
				case ID_DRAW_LINE:
					if (!line)
					{
						line=true, rec=false, ell=false, text=false, select=false;
						wsprintf(msg,L"You chose LINE!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
					}
					else
					{
						line=false;
						wsprintf(msg,L"You unchose LINE!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
					}
					break;
				case ID_DRAW_RECTANGLE:
					if (!rec)
					{
						line=false, rec=true, ell=false, text=false, select=false;
						wsprintf(msg,L"You chose RECT!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_RECTANGLE, MF_CHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
					}
					else
					{
						rec=false;
						wsprintf(msg,L"You unchose RECT!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
					}
					break;
				case ID_DRAW_ELLIPSE:
					if (!ell)
					{
						line=false, rec=false, ell=true, text=false, select=false;
						wsprintf(msg,L"You chose ELLI!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_ELLIPSE, MF_CHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
					}
					else
					{
						ell=false;
						wsprintf(msg,L"You unchose ELLI!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
					}
					break;
				case ID_DRAW_TEXT:
					if (!text)
					{
						line=false, rec=false, ell=false, text=true, select=false;
						wsprintf(msg,L"You chose TEXT!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_TEXT, MF_CHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
					}
					else
					{
						text=false;
						wsprintf(msg,L"You unchose TEXT!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
					}
					break;
				case ID_DRAW_SELECTOBJECT:
					if (!select)
					{
						line=false, rec=false, ell=false, text=false, select=true;
						wsprintf(msg,L"You chose SELECT");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_SELECTOBJECT, MF_CHECKED | MF_BYCOMMAND);
					}
					else
					{
						select=false;
						wsprintf(msg,L"You unchose SELECT!");
						MessageBox(hWnd,msg,L"NOTIFICATION",MB_OK);
						CheckMenuItem(GetMenu(hWnd), ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
					}
					break;
				case ID_FORMAT_CHOOSEFONT:
					Font(hWnd);
					break;
				case ID_FORMAT_CHOOSECOLOR:
					Color(hWnd);
					break;
				case ID_TOOLBAR_VIEWHIDE:
					if (GetClientRect(hWnd,&rc)) ViewHideToolbar(hWnd,hwndMDIClient,lParam,rc,toolbar);
					break;
				case IDM_ABOUT:
					DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				default:
					return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
			}
			break;
		case WM_SIZE:
			if (GetClientRect(hWnd,&rc))
			{
				UINT w, h;
				w = LOWORD(lParam);
				h = HIWORD(lParam);
				MoveWindow(hwndMDIClient, 0, 0, w, h, TRUE);
				if (toolbar) ViewHideToolbar(hWnd,hwndMDIClient,lParam,rc,toolbar);
			}
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//---------------------------------------QUOCTRAN---------------------------------------

void NewBitmapWnd(int id)
{
	wsprintf(szBitmapTitle, L"Noname-%d.drw", id);
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = szBitmapWindowClass;
	mdiCreate.szTitle = szBitmapTitle;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LONG)(LPMDICREATESTRUCT)&mdiCreate);
}

LRESULT CALLBACK BitmapChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int x,y;
	TCHAR msg[50];
	HDC hdc = GetDC(hWnd);
	HWND hEdit;

	switch (message)
	{
		case WM_COMMAND: 
			return 0;
		case WM_MDIACTIVATE:
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		case WM_PAINT:
			PAINTSTRUCT ps;
			hdc = BeginPaint(hWnd, &ps);
			EndPaint(hWnd, &ps);
			return 0;
		case WM_LBUTTONDOWN:
			x = LOWORD(lParam);
			y = HIWORD(lParam);
			Ellipse(hdc,x-100,y-100,x+100,y+100);
			return 0;
		default:
            return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK MDICloseProc(HWND hMDIWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hMDIWnd, 0L);
	return 1;
}

void Color(HWND hWnd)
{
	CHOOSECOLOR cc; 
	COLORREF acrCustClr[16]; 
	DWORD rgbCurrent = RGB(255, 0, 0); 
									 
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd; 
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent; 
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	if (ChooseColor(&cc))
	{
		rgbCurrentColor = cc.rgbResult;
	}
}

void Font(HWND hWnd)
{
	CHOOSEFONT cf;
	LOGFONT lf;
	HFONT hfNew;

	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &lf;
	cf.iPointSize = 0;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

	ChooseFont(&cf);
	hfNew = CreateFontIndirect(cf.lpLogFont);
	SelectObject(GetDC(hWnd), hfNew);	
	rgbCurrentColor = cf.rgbColors;
}

void CreateToolBar(HWND hWnd)
{
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_HELP,	ID_DRAW_LINE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_HELP,	ID_DRAW_RECTANGLE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_HELP,	ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_HELP,	ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_HELP,	ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	//=================================================================================================
	//--------------------------------------CACH TAO TOOLBAR KHAC--------------------------------------
	//=================================================================================================
	
	/*InitCommonControls();
	hToolBarWnd = CreateWindowEx(0, TOOLBARCLASSNAME, NULL, WS_CHILD | WS_VISIBLE, 0, 0, 0, 0,
       hWnd, (HMENU)IDC_PROCJECT1, GetModuleHandle(NULL), NULL);
	 SendMessage(hToolBarWnd, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);

	TBADDBITMAP tbab;
	 tbab.hInst = HINST_COMMCTRL;
    tbab.nID = IDB_STD_SMALL_COLOR;
    SendMessage(hToolBarWnd, TB_ADDBITMAP, 0, (LPARAM)&tbab);
	
	TBBUTTON tbb[3];
	ZeroMemory(tbb, sizeof(tbb));
    tbb[0].iBitmap = STD_FILENEW;
    tbb[0].fsState = TBSTATE_ENABLED;
    tbb[0].fsStyle = TBSTYLE_BUTTON;
    tbb[0].idCommand = ID_FILE_NEW;

    tbb[1].iBitmap = STD_FILEOPEN;
    tbb[1].fsState = TBSTATE_ENABLED;
    tbb[1].fsStyle = TBSTYLE_BUTTON;
    tbb[1].idCommand = ID_FILE_OPEN;

    tbb[2].iBitmap = STD_FILESAVE;
    tbb[2].fsState = TBSTATE_ENABLED;
    tbb[2].fsStyle = TBSTYLE_BUTTON;
    tbb[2].idCommand = ID_FILE_SAVE;
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, sizeof(tbb)/sizeof(TBBUTTON), (LPARAM)&tbb);*/
}
void ViewHideToolbar(HWND hWnd, HWND hwndMDIClient, LPARAM lParam, RECT rc,bool &IsUsing)
{
	UINT w, h;
	w = LOWORD(lParam);
	h = HIWORD(lParam);

	if (IsUsing)
	{
		MoveWindow(hwndMDIClient, 0, 0, rc.right, rc.bottom, TRUE);
		ShowWindow(hToolBarWnd, SW_HIDE);
		CheckMenuItem(GetMenu(hWnd), ID_TOOLBAR_VIEWHIDE, MF_UNCHECKED | MF_BYCOMMAND);
		IsUsing=false;
	}
	else
	{
		CreateToolBar(hWnd);
		MoveWindow(hwndMDIClient, 0, rc.top+28, rc.right, rc.bottom, TRUE);
		ShowWindow(hToolBarWnd, SW_SHOW);
		CheckMenuItem(GetMenu(hWnd), ID_TOOLBAR_VIEWHIDE, MF_CHECKED | MF_BYCOMMAND);
		IsUsing=true;
	}
}

