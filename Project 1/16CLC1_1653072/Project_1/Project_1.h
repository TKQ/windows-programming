#pragma once

#include "resource.h"

void NewBitmapWnd(int id);
void Color(HWND hWnd);
void Font(HWND hWnd);
void CreateToolBar(HWND hWnd);
void ViewHideToolbar(HWND hWnd, HWND hwndMDIClient, LPARAM lParam,RECT rc,bool &IsUsing);

