//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Project_1.rc
//
#define IDC_MYICON                      2
#define IDD_PROJECT_1_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_PROJECT_1                   107
#define IDI_SMALL                       108
#define IDC_PROJECT_1                   109
#define IDS_BITMAP_TITLE                110
#define IDC_BITMAP_CLASS                111
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     134
#define IDB_LINE                        135
#define IDI_ICON1                       136
#define IDI_X                           136
#define ID_FILE_NEW                     32771
#define ID_FILE_OPEN                    32772
#define ID_FILE_SAVE                    32773
#define ID_DRAW_LINE                    32774
#define ID_DRAW_RECTANGLE               32775
#define ID_DRAW_ELLIPSE                 32776
#define ID_DRAW_TEXT                    32777
#define ID_DRAW_SELECTOBJECT            32778
#define ID_DRAW_FORMAT                  32779
#define ID_FORMAT_CHOOSECOLOR           32780
#define ID_FORMAT_CHOOSEFONT            32781
#define ID_WINDOW_TIDEVERTICAL          32787
#define ID_WINDOW_TIDEHORIZONTAL        32788
#define ID_WINDOW_CASCADE               32789
#define ID_WINDOW_CLOSEALL              32790
#define ID_TOOLBAR_VIEWHIDE             32792
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
