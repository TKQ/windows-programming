Ho Ten: Tran Kien Quoc
MSSV: 1653072
Lop: 16CLC1
Phien ban Visual Studio su dung: 2010
--
PROJECT 01 - HOAN THANH TAT CA YEU CAU
1/ Tao duoc cac child window moi voi title la "Noname - X.drw" voi X=1,2,3,...
2/ Hien thi duoc messagebox "You chose OPEN!" va "You chose SAVE!".
3/ Da them duoc cac phim tat Ctrl + N, Ctrl + S, Ctrl + O nhung ma phim tat khong chay duoc. 
Khong hieu luon, Huhu :(
4/ Co Exit de thoat ung dung.
5/ Mo duoc hop thoai Color va Font cho phep user chon.
6/ Line, Rectangle, Ellipse, Text, Select object: Line duoc check default khi chuong trinh 
khoi dong. Chon chuc nang nao thi chuc nang do duoc check va thuc hien, con lai se la uncheck.
7/ Tao duoc cac child window va user co the thuc hien chuc nang Tide (Vertical or Horizontal),
Cascade va Close all.
8/ Co TOOLBAR.
*Luu y: 
1/ Khi resize frame window thi se mat Toolbar (Thuc ra Toolbar bi HIDE di), chi can mo Toolbar 
lai la xong.
2/ Vi khong the nao them resouce cac loai anh bitmap vao nen cac chuc nang Line, Rectangle,
Ellipse, Text, Select object se duoc gan anh mac dinh la STD_HELP (Anh bitmap he thong).